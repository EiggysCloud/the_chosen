﻿using UnityEngine;

public class InventoryManager : MonoBehaviour
{

    public bool LefthandInPlace = false;
    public bool RighthandInPlace = false;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LeftHand"))
        {
            LefthandInPlace = true;
        }
        else if (other.CompareTag("RightHand"))
        {
            RighthandInPlace = true;
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("LeftHand"))
        {
            LefthandInPlace = false;
        }
        else if (other.CompareTag("RightHand"))
        {
            RighthandInPlace = false;
        }
    }


}
