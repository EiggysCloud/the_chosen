﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public AudioClip[] ballSoundClips;
    public AudioSource BallAudioSource;
    GameObject paddle;

    void Start()
    {
        paddle = GameObject.Find("Sword");
        BallAudioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject == paddle)
        {
            BallAudioSource.clip = ballSoundClips[0];
            BallAudioSource.Play();
        }
        else if (other.gameObject.CompareTag("Table"))
        {
            BallAudioSource.clip = ballSoundClips[1];
            BallAudioSource.Play();
        }

    }




}
