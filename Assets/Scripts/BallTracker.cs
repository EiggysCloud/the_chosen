﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallTracker : MonoBehaviour
{
    public Text TotalBallTracker;
    public int TotalBalls;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        TotalBallTracker.text = TotalBalls + "";
    }
}
