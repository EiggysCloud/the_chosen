﻿using UnityEngine;

public class Bilboard : MonoBehaviour
{
    GameObject player;
    private void Start()
    {

        player = GameObject.Find("CenterEyeAnchor");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform);
    }
}
