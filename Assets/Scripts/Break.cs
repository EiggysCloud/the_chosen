﻿using UnityEngine;

public class Break : MonoBehaviour
{
    Rigidbody m_Rigidbody;

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SwordBlade"))
        {
            m_Rigidbody.constraints = RigidbodyConstraints.None;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
