﻿using UnityEngine;

public class Footsteps : MonoBehaviour
{

    public AudioClip[] FootStepAudioClips;
    public AudioSource FeetAudioSource;
    CharacterController PlayerController;

    bool moving;

    private void Start()
    {
        PlayerController = gameObject.GetComponent<CharacterController>();

    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();

        if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickUp) | OVRInput.Get(OVRInput.Button.PrimaryThumbstickDown) | OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft) | OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight))
        {
            moving = true;
        }
        else
        {
            moving = false;
        }


        if (moving == true && FeetAudioSource.isPlaying == false && PlayerController.isGrounded == true)
        {
            FeetAudioSource.Play();
            FeetAudioSource.clip = FootStepAudioClips[Random.Range(0, 6)];
        }
    }
}
