﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityManager : MonoBehaviour
{
    [SerializeField]
    int Gravity = -8;
    TextMesh textObject;

    private void Start()
    {
        textObject = GameObject.Find("GravityValue").GetComponent<TextMesh>();
        Physics.gravity = new Vector3(0, Gravity, 0);
        textObject.text = "Gravity: " + Gravity;
    }

    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.name == "Increase")
        {
            Gravity++;
            Physics.gravity = new Vector3(0, Gravity, 0);
            textObject.text = "Gravity: " + Gravity;
        }
        if (other.gameObject.name == "Decrease")
        {
            Gravity--;
            Physics.gravity = new Vector3(0, Gravity, 0);
            textObject.text = "Gravity: " + Gravity;
        }

        if (gameObject.name == "ResetButton")
        {
            print("contact " + other.gameObject.tag);

            if (other.gameObject.CompareTag("LeftHand") || other.gameObject.CompareTag("RightHand"))
            {
                Gravity = -8;
                Physics.gravity = new Vector3(0, Gravity, 0);
                textObject.text = "Gravity: " + Gravity;
            }
        }
    }
}



