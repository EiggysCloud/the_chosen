﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabBall : MonoBehaviour
{

    GameObject bol;
    GameObject boll;
    GameObject bolll;
    GameObject Paddle;
    // Start is called before the first frame update
    void Start()
    {
        bol = GameObject.Find("Ball1");
        boll = GameObject.Find("Ball2");
        bolll = GameObject.Find("Ball3");
        Paddle = GameObject.Find("Sword");
    }

    void Update()
    {
        OVRInput.Update();

        if (OVRInput.Get(OVRInput.Button.One))
        {
            PaddleRetrive();
        }
        if (OVRInput.Get(OVRInput.Button.Two))
        {
            BallTwo();
        }
        if (OVRInput.Get(OVRInput.Button.Four))
        {
            BallThree();
        }

        if (OVRInput.Get(OVRInput.Button.Three))
        {
            BallOne();
        }

    }


        void BallOne()
    {
        if (OVRInput.Get(OVRInput.Button.Three))
        {
            bol.transform.parent = gameObject.transform;
            bol.transform.localPosition = new Vector3(0, 0, 0);
            bol.transform.parent = null;
            bol.transform.localScale = new Vector3(.03f, .03f, .03f);
        }
    }

    void BallTwo()
    {
        if (OVRInput.Get(OVRInput.Button.Two))
        {
            boll.transform.parent = gameObject.transform;
            boll.transform.localPosition = new Vector3(0, 0, 0);
            boll.transform.parent = null;
            boll.transform.localScale = new Vector3(.03f, .03f, .03f);

        }
    }

    void BallThree()
    {
        if (OVRInput.Get(OVRInput.Button.Four))
        {
            bolll.transform.parent = gameObject.transform;
            bolll.transform.localPosition = new Vector3(0, 0, 0);
            bolll.transform.parent = null;
            bolll.transform.localScale = new Vector3(.03f, .03f, .03f);
        }
    }

    void PaddleRetrive()
    {
        if (OVRInput.Get(OVRInput.Button.One))
        {
            Paddle.transform.parent = gameObject.transform;
            Paddle.transform.localPosition = new Vector3(0, 0, 0);
            Paddle.transform.parent = null;
            Paddle.transform.localScale = new Vector3(0.3521205f, 0.3521205f, 0.4287066f);
        }
    }
}
