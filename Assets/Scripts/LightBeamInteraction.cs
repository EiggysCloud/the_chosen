﻿using UnityEngine;

public class LightBeamInteraction : MonoBehaviour
{

    public GameObject beam;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            beam.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            beam.GetComponent<MeshRenderer>().enabled = true;
        }
    }


}
