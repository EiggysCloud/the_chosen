﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GetSetScore : MonoBehaviour
{
    public Text ScoreBoard;

    public int TotalScore;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ScoreBoard.text = TotalScore + "";
    }
}
