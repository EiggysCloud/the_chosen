﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialSpawn : MonoBehaviour
{

    [SerializeField]
    GameObject ScenemasterPrefab;
    Vector3 place = new Vector3(16.27f, 2.86f, 0.14f);
    GameObject Scenemaster;

    // Start is called before the first frame update
    void Start()
    {
        Scenemaster = GameObject.Find("GameMaster(Clone)");
        if (Scenemaster == null)
        {
            Scenemaster = Instantiate(ScenemasterPrefab, place, Quaternion.identity);
            DontDestroyOnLoad(Scenemaster);
        }

    }


}
