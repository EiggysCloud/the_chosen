﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinColorApply : MonoBehaviour
{
    GameObject LeftHand;
    GameObject RightHand;
    Renderer L_Renderer;
    Renderer R_Renderer;

    //Set the Color to white to start off
    public Color color = Color.white;

    void Start()
    {
        LeftHand = GameObject.Find("hands:Lhand");
        LeftHand = GameObject.Find("hands:Rhand");

        //Fetch the Renderer of the GameObject
        L_Renderer = LeftHand.GetComponent<Renderer>();
        R_Renderer = RightHand.GetComponent<Renderer>();
    }

    private void update()
    {
        //Sliders for the red, green and blue components of the Color
        color.r = transform.position.x;
        color.g = transform.position.y;
        color.b = transform.position.z;

        print(color.b);
        //Set the Color of the GameObject's Material to the new Color
        R_Renderer.material.color = color;
        L_Renderer.material.color = color;
    }
}