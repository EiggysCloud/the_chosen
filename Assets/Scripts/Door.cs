﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    [SerializeField]
    string Target;
    GameObject theHero;

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            theHero = other.gameObject;
            SceneManager.LoadScene(Target);
            Physics.SyncTransforms();

  //          Whereto();
        }
    }

/*
    private void Whereto()
    {
        if (Target == "TheChosenSword")
        {
            theHero.transform.position = new Vector3(14.07568f, 1.473f, 1.913783f);
        }
        if (Target == "PingPongPower")
        {
            theHero.transform.position = new Vector3(0.427f, 2.6f, 3.994f);
        }

    }

    */
}
