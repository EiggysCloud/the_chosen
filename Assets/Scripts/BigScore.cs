﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigScore : MonoBehaviour
{

    [SerializeField]
    // private Pickup myPickup;
    private GetSetScore GetSetScores;

    public int value;



    void OnCollisionExit(Collision other)
    {

            GetSetScores.TotalScore -= value;
}

    void OnCollisionEnter(Collision other)
    {
            GetSetScores.TotalScore += value;

    }
}


